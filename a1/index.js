const users = [
  { username:  'johndoe', password: 'johndoe123' },
  { username:  'alice', password: 'alicepassword' },
  { username:  'bob', password: '123bob' },
  { username:  'cindy', password: 'cincincindy' },
]

const express = require('express');
const app = express();

const PORT = 3000;

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ROUTES
/*
  1. Create a GET route that will access the "/home" route
      1.1 It should print the message: 'Welcome to the home page'
      1.2 Check the results on Postman
      1.3 Screenshot the result
      1.4 Make lagay the screenshot in a1 folder
*/
app.get('/home', (request, response) => {
  response.send('Welcome to the home page');
});

/*
  2. Create a GET route that will access the "/users"
      2.1 It should print the users
      2.2 Check the result on Postman
      2.3 Screenshot the result
      2.4 Make lagay the screenshot in a1 folder
*/
app.get('/users', (request, response) => {
  response.json(users);
});

/*
  3. Create a delete route that will access the "/delete-user" route
      3.1 It should print "The user has been deleted"
      3.2 Check the result on Postman
      3.3 Screenshot the result
      3.4 Make lagay the screenshot in a1 folder
*/
app.delete('/delete-user', (request, response) => {
  const target = request.body.username;

  for (let i = 0; i < users.length; i++) {
    if (users[i].username === target) {
      users.splice(i, 1);
      response.send(`The user ${target} has been deleted.`)
      return;
    }
  }

  response.send(`User ${target} not found.`)
});

app.post('/signup', (request, response) => {
  const newUser = {
    username: request.body.username,
    password: request.body.password
  }

  users.push(newUser)
  response.json(users);
});

app.listen(PORT, () => { console.log(`Server running in port ${PORT} [http://localhost:${PORT}]`) });
